# Quadric Edge Collapse

Algorithm to simplify triangle mesh by quadric.
In other words: Reduce the amount of triangles in model.

C++ implementation of quadric edge collapse algorithm. Main aim is to be fast as possible in processing triangle mesh. Second aim is create  easy integration into your application. This could eliminae Carbon footprint of applications and create better being.

## Description

Algorithm idea is calculate error for each edge in model. Error represents how much it will changed the model when this edge became to the new vertex. Than sorts errors from small to big into queue and start reducing until end conditions is done.

Example of collapse one edge. Bold edge between vertex `v1` and `v2` collapse(became) to the new vertex `v`.

![One edge collapse](resources/OneEdgeCollapse.png)

- Source paper [[pdf](https://people.eecs.berkeley.edu/~jrs/meshpapers/GarlandHeckbert2.pdf), [local](resources/GarlandHeckbert2.pdf)]
- Sum up page [[link](https://users.csc.calpoly.edu/~zwood/teaching/csc570/final06/jseeba/)]
- Inspiration for fast implementation [[github](https://github.com/sp4cerat/Fast-Quadric-Mesh-Simplification)]

## Compare
- [ ] MeshLab
- [ ] LibIgl
- [ ] sp4cerat/Fast-Quadric-Mesh-Simplification
- [ ] CGAL

## Compile
Compile by CMake
```
mkdir build
cd build
cmake ..
```

## List of application already use it
Prusa slicer,

## Support
For support create issue

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Project status
Work in progress

## TODO: Test and Deploy
- [ ] Add built-in continuous integration in GitLab.
- [ ] Add Performance test.
- [ ] Add badges - compile on Win, OsX, Lin, test, ...
- [ ] Prepare enviroment for merge request - checking code style, static analyze
- [ ] Automatic release ( Conan? )
- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:eec075bc9a6c95c8bdba1757f84f992e?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:eec075bc9a6c95c8bdba1757f84f992e?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:eec075bc9a6c95c8bdba1757f84f992e?https://docs.gitlab.com/ee/user/clusters/agent/)
