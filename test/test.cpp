#include "gtest/gtest.h"
#include "../src/QuadricEdgeCollapse.hpp"

using namespace qec;
/*
namespace Private {

struct Similarity
{
    float max_distance     = 0.f;
    float average_distance = 0.f;

    Similarity() = default;
    Similarity(float max_distance, float average_distance)
        : max_distance(max_distance), average_distance(average_distance)
    {}
};

// border for our algorithm with frog_leg model and decimation to 5%
Similarity frog_leg_5(0.32f, 0.043f);

Similarity get_similarity(const TriangleMesh &from, const TriangleMesh &to)
{
    // create ABBTree
    auto tree = AABBTreeIndirect::build_aabb_tree_over_indexed_triangle_set(
        from.vertices, from.indices);
    float sum_distance = 0.f;

    float max_distance      = 0.f;
    auto  collect_distances = [&](const Vec3f &surface_point) {
        size_t hit_idx;
        Vec3f  hit_point;
        float  distance2 =
            AABBTreeIndirect::squared_distance_to_indexed_triangle_set(
                from.vertices, from.indices, tree, surface_point, hit_idx,
                hit_point);
        float distance = sqrt(distance2);
        if (max_distance < distance) max_distance = distance;
        sum_distance += distance;
    };

    for (const Vec3f &vertex : to.vertices) { collect_distances(vertex); }
    for (const Vec3i &t : to.indices) {
        Vec3f center(0, 0, 0);
        for (size_t i = 0; i < 3; ++i) { center += to.vertices[t[i]] / 3; }
        collect_distances(center);
    }

    size_t count            = to.vertices.size() + to.indices.size();
    float  average_distance = sum_distance / count;

    std::cout << "max_distance = " << max_distance
              << ", average_distance = " << average_distance << std::endl;
    return Similarity(max_distance, average_distance);
}

void is_better_similarity(const indexed_triangle_set &its_first,
                          const indexed_triangle_set &its_second,
                          const Similarity &          compare)
{
    Similarity s1 = get_similarity(its_first, its_second);
    Similarity s2 = get_similarity(its_second, its_first);

    CHECK(s1.average_distance < compare.average_distance);
    CHECK(s1.max_distance < compare.max_distance);
    CHECK(s2.average_distance < compare.average_distance);
    CHECK(s2.max_distance < compare.max_distance);
}

void is_worse_similarity(const indexed_triangle_set &its_first,
                         const indexed_triangle_set &its_second,
                         const Similarity &          compare)
{
    Similarity s1 = get_similarity(its_first, its_second);
    Similarity s2 = get_similarity(its_second, its_first);

    if (s1.max_distance < compare.max_distance &&
        s2.max_distance < compare.max_distance)
        CHECK(false);
}

bool exist_triangle_with_twice_vertices(
    const std::vector<stl_triangle_vertex_indices> &indices)
{
    for (const auto &face : indices)
        if (face[0] == face[1] || face[0] == face[2] || face[1] == face[2])
            return true;
    return false;
}

} // namespace Private*/

TEST(integration, simplify)
{	
	TriangleMesh tm;
    tm.vertices = {Vec3f(-1.f, 0.f, 0.f), Vec3f(0.f, 1.f, 0.f),
                    Vec3f(1.f, 0.f, 0.f), Vec3f(0.f, 0.f, 1.f),
                    // vertex to be removed
                    Vec3f(0.9f, .1f, -.1f)};
    tm.triangles = {Vec3i(1, 0, 3), Vec3i(2, 1, 3), Vec3i(0, 2, 3),
                   Vec3i(0, 1, 4), Vec3i(1, 2, 4), Vec3i(2, 0, 4)};
    // edge to remove is between vertices 2 and 4 on trinagles 4 and 5

    TriangleMesh tm2 = tm; // copy
    // its_write_obj(its, "tetrhedron_in.obj");
    uint32_t wanted_count = static_cast<uint32_t>(tm.triangles.size() - 1);
    simplify(tm, wanted_count);
    // its_write_obj(its, "tetrhedron_out.obj");
    EXPECT_EQ(tm.triangles.size(), 4);
    EXPECT_EQ(tm.vertices.size(), 4);

    for (size_t i = 0; i < 3; i++)
        EXPECT_EQ(tm.triangles[i], tm2.triangles[i]);
    
    for (size_t i = 0; i < 4; i++) {
        if (i == 2) continue;
        EXPECT_EQ(tm.vertices[i], tm2.vertices[i]);
    }

    const Vec3f &v  = tm.vertices[2];  // new vertex
    const Vec3f &v2 = tm2.vertices[2]; // moved vertex
    const Vec3f &v4 = tm2.vertices[4]; // removed vertex
    for (size_t i = 0; i < 3; i++) {
        bool is_between = (v[i] < v4[i] && v[i] > v2[i]) ||
                          (v[i] > v4[i] && v[i] < v2[i]);
        EXPECT_TRUE(is_between);
    }
}
