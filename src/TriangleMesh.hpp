#ifndef TriangleMesh_hpp_
#define TriangleMesh_hpp_

#define use_default_structure
#ifdef use_default_structure
#include <array>
#include <vector>
#include <assert.h>
namespace qec {
template<typename DataType> class Data3
{
    std::array<DataType, 3> data;
public:
    Data3(DataType x, DataType y, DataType z) : data({x, y, z}) {}
    Data3(const Data3 &) = default;
    Data3(Data3 &&)      = default;
    Data3 &operator=(const Data3 &) = default;
    Data3 &operator=(Data3 &&) = default;
    virtual ~Data3()           = default;

    DataType &x() noexcept { return data[0]; };
    DataType &y() noexcept { return data[1]; };
    DataType &z() noexcept { return data[2]; };

    const DataType &x() const noexcept { return data[0]; };
    const DataType &y() const noexcept { return data[1]; };
    const DataType &z() const noexcept { return data[2]; };

    DataType &operator[](std::size_t idx)
    {
        assert(idx < 3);
        return data[idx];
    }
    const DataType &operator[](std::size_t idx) const
    {
        assert(idx < 3);
        return data[idx];
    }

    template<typename DataTypeOut> Data3<DataTypeOut> cast() const
    {
        return Data3<DataTypeOut>(static_cast<DataTypeOut>(x()),
                                  static_cast<DataTypeOut>(y()),
                                  static_cast<DataTypeOut>(z()));
    }

    Data3<DataType> operator+(const Data3<DataType> &a) const {
        return Data3<DataType>(x() + a.x(), y() + a.y(), z() + a.z()); }
    Data3<DataType> operator-(const Data3<DataType> &a) const {
        return Data3<DataType>(x() - a.x(), y() - a.y(), z() - a.z()); }
    template<typename T>
    Data3<DataType> operator/(const T &a) const {
        return Data3<DataType>(x()/a, y()/a, z()/a); }
    bool operator==(const Data3<DataType> &a) const {
        return x() == a.x() && y() == a.y() && z() == a.z(); }
    
    void normalize(){
        double div = sqrt(x() * x() + y() * y() + z() * z());
        x() = static_cast<DataType>(x() / div);
        y() = static_cast<DataType>(y() / div);
        z() = static_cast<DataType>(z() / div);
    }

    Data3<DataType> cross(const Data3<DataType> &a) const {
        return Data3<DataType>(
            y() * a.z() - z() * a.y(),
            -x() * a.z() + z() * a.x(),
            x() * a.y() - y() * a.x());
    }

    DataType dot(const Data3<DataType> &a) const {
        return x() * a.x() + y() * a.y() + z() * a.z();
    }
};

using Vec3f = Data3<float>;
using Vec3d = Data3<double>;
using Vec3i = Data3<int>;
} // namespace qec

#endif // use_default_structure

//#define USE_EIGEN
#ifdef USE_EIGEN
#include "Eigen/Geometry"
#endif

namespace qec {

#ifdef USE_EIGEN
using Vec3i = Eigen::Matrix<uint32_t, 3, 1, Eigen::DontAlign>;
using Vec3f = Eigen::Matrix<float, 3, 1, Eigen::DontAlign>;
using Vec3d = Eigen::Matrix<double, 3, 1, Eigen::DontAlign>;
#endif

using Vertex    = Vec3f;
using Vertices  = std::vector<Vertex>;
using Triangle  = Vec3i; // indices to vertices
using Triangles = std::vector<Triangle>;

/// <summary>
/// Data structure to hold surface mesh defined by space points(vertices)
/// and triplets of indicies defining surface
/// </summary>
struct TriangleMesh
{
    // set of unique points in spatial(3d) space
    Vertices vertices;
    // connection of vertices into triangles, index into vertices
    Triangles triangles;
};

} // namespace qec

#endif // TriangleMesh_hpp_
