#ifndef QuadricEdgeCollapse_hpp_
#define QuadricEdgeCollapse_hpp_

#include <functional>
#include "TriangleMesh.hpp"

// abbreviation of Quadric edge collapse
namespace qec{

using ThrowOnCancel = std::function<void(void)>;
using StatusFn = std::function<void(int)>;

/// <summary>
/// Simplify mesh by Quadric metric, from lowest impact edge
/// </summary>
/// <param name="mesh">IN/OUT triangle mesh to be simplified.</param>
/// <param name="triangle_count">Wanted triangle count. Could differ to result count.</param>
/// <param name="max_error">Maximal Quadric for reduce.
/// When nullptr then max float is used
/// Output: Last used ErrorValue to collapse edge</param>
/// <param name="throw_on_cancel">Could stop process of calculation.</param>
/// <param name="statusfn">Give a feed back to user about progress. Values 1 - 100</param>
void simplify(
	TriangleMesh& mesh,
    uint32_t      triangle_count  = 0,
    float *       max_error       = nullptr,
    ThrowOnCancel throw_on_cancel = nullptr,
    StatusFn      statusfn        = nullptr);

} // namespace qec

#endif // QuadricEdgeCollapse_hpp_
